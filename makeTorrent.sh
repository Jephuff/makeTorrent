#!/bin/sh
#
# makeTorrent.sh by AirCombat
#
# Usage: ./makeTorrent.sh <source directory/file> <output .torrent name> [optional category]
#
# First thing you need to do is put your announce url in the announce
# variable right after this comment.
#
# If you build mktorrent from source, then it will be installed to
# /usr/local/bin. If you install it from package it very well might
# be somewhere else. Do which mktorrent and then replace bin
# with whatever output you get.
#
# This is a wrapper around mktorrent for members of REDacted.ch and 
# MyAnonaMouse. Others are easily added (see below)
# Just pass it the source file/directory and the name for the output torrent file
# You need to provide the whole name, including .torrent. Make sure spaces and
# special characters (ie "-", " ") are properly escaped with backslashes.
# Most shells will do this for you if you use TAB to autocomplete.
#
# You must specify which tracker to use. You can add more very easily by
# creating a variable for the tracker name and then adding more options
# to the if statement on line 106. 
#
# You can optionally give a label/category which will be used when adding the torrent
# to qbittorrent
#
# Released under the MIT License

announcered="https://flacsfor.me/xxxx/announce"
# announcemam="https://t.myanonamouse.net/tracker.php/xxxx/announce"
# announcetest="https://example.com/tracker/announce"
bin="/usr/local/bin/mktorrent"

# DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # get the current directory
DIR=$(cd "$(dirname -- "$0")" && pwd)

#check for mktorrent
if ! type mktorrent 2>/dev/null 1>&2; then
    echo " --- mktorrent not installed, downloading and updating ---"
    cd /tmp
    git clone https://github.com/Rudde/mktorrent.git
    cd mktorrent
    make
    if (( $? != 0 )); then
	echo " --- Problem with compilation! Exiting... ---"
	exit 1
    fi
    sudo -s <<EOF
make install
EOF
    if (( $? != 0 )); then
	echo " --- Problem installing! Exiting... ---"
	exit 1
    fi
    echo " --- mktorrent successfully installed! Continuing with torrent creation... ---"
    cd ..
    rm -rf mktorrent/
elif type mktorrent 2>/dev/null 1>&2; then
    # check to see if the version has changed or create has if it's not already there
    # this is a bit of a hack, we check the hash of the changelog file to determine
    # if there have been any major changes
    cd /tmp
    rm CHANGELOG.md #ensure newest
    wget -q https://raw.githubusercontent.com/Rudde/mktorrent/master/CHANGELOG.md
    if [[ ! -e "$DIR/hash.txt" ]]; then
	sha1sum /tmp/CHANGELOG.md > $DIR/hash.txt
    fi
    sha1sum /tmp/CHANGELOG.md > $DIR/hashnew.txt
    diff $DIR/hash.txt $DIR/hashnew.txt
    if (( $? == 0 )); then
	# we have the latest version
	echo " --- We have the latest version --- "
    else
	echo " --- New version of mktorrent available! Cloning and installing... ---"
	# get and update to the latest version
	cd /tmp
	git clone https://github.com/Rudde/mktorrent.git
	cd mktorrent
	make
	if (( $? != 0 )); then
	    echo " --- Problem with compilation! Exiting... ---"
	    exit 1
	fi
	sudo -s <<EOF
make install
EOF
	if (( $? != 0 )); then
	    echo " --- Problem installing! Exiting... ---"
	    exit 1
	fi
	mv $DIR/hashnew.txt $DIR/hash.txt
	echo " --- mktorrent successfully updated! Continuing with torrent creation... ---"
	cd ..
	rm -rf mktorrent/
    fi
fi

if [[ $# -ge 2 ]]; then
    source=$1
    output=$2
    # tracker=$3
else
    echo "Usage: ./makeTorrent.sh <source file/directory> <output torrent> [category]"
fi

# if [ "$tracker" = "RED" ]; then
    announce=$announcered
    option="-s RED"
# elif [ "$tracker" = "MAM" ]; then
#     announce=$announcemam
#     option="-s mam"
# elif [ "$tracker" = "TEST" ]; then
#     announce=$announcetest
#     option="-s test"
# else
#     echo "No tracker or incorrect tracker given! Quitting..."
#     exit 1
# fi

if [[ -e "$source" || -d "$source" ]]; then # check is source is an existing file or directory
    size=$( du -m -c "$source" | tail -1 | grep -Eo ^[0-9]+ )
    case $(( size > 0 && size <= 69 ? 1 :
	     size >= 62 && size <= 138 ? 2 :
	     size >= 125 && size <= 275 ? 3 :
	     size >= 250 && size <= 550 ? 4 :
	     size >= 500 && size <= 1100 ? 5 :
	     size >= 1000 && size <= 2200 ? 6 :
	     size >= 1950 && size <= 4300 ? 7 :
	     size >= 3900 && size <= 8590 ? 8 :
	     size >= 7810 ? 9 : 0)) in
	(1) chunk=15;;
	(2) chunk=16;;
	(3) chunk=17;;
	(4) chunk=18;;
	(5) chunk=19;;
	(6) chunk=20;;
	(7) chunk=21;;
	(8) chunk=22;;
	(9) chunk=23;;
	(0) echo " --- Invalid file size! ----"
	    exit 1;;
    esac
    echo " --- Using chunk size:" $chunk "---"
    cd $DIR
    rm "$output"
    $bin -l $chunk -p $option -a $announce "$source" -o "$output"
    if (( $? == 0 )); then
	echo " --- Torrent successfully created at $output! Exiting... ---"
	echo " --- Adding torrent to qBittorrent ---"
	if [[ -z "$3" ]]; then
	    ./qbit.py "$source" "$output" "Uncategorized"
	else
	    ./qbit.py "$source" "$output" $3
	fi
	exit 0
    else
	echo " --- Problem creating torrent! Exiting... ---"
	exit 1
    fi
else
    echo " --- Source file/directory does not exist! ---"
    exit 1
fi
