# makeTorrent
## by AirCombat
### with changes from lazor

This is a fairly simple BASH wrapper around mktorrent which takes a lot of the
thinking out of making torrents. It can be adapted to any tracker which uses
consistent announce URLs for its users - private or public.

The usage is in the comment at the top of the BASH script itself, but the
gist of it is:
`./makeTorrent.sh <input file/directory> <output.torrent> <tracker> [qbittorrent category]`

Where <> is mandatory and [] is optional. 
There are a couple small bugs that I have yet to fix but it should work great
for any general purpose usage.

To add a tracker, simply create another variable with the announce URL, then
add another elif case to the possible tracker choices. Anyone with any
programming experience should find this trivial to do.